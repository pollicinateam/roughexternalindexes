cos.sim <- function(v1, v2){
  res = v1 %*% v2
  res = res / (norm(v1) * norm(v2))
  return(res)
}

max.sim <- function(v1, v2){
  return(max(v1 * v2))
}

norm <- function(v){
  return(sqrt(sum(v^2)))
}

prod <- function(v1, v2){
  return(v1 %*% v2)
}
